package com.bren.dao;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

public interface CrudDao<T> {

    void save(T t);

    void update(T t, Long id);

    List<T> findAll();

    T findById(Long id);

    void delete(Long id);
}
