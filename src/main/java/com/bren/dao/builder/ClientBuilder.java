package com.bren.dao.builder;

import com.bren.entity.Client;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ClientBuilder implements Builder<Client> {

    @Override
    public Client build(ResultSet resultSet) {
        Client client = new Client();
        try {
            client.setName(resultSet.getString("name"));
            client.setLastName(resultSet.getString("last_name"));
            client.setCityId(resultSet.getLong("city_id"));
            client.setAge(resultSet.getInt("age"));
            client.setPhone(resultSet.getString("phone"));
            client.setEmail(resultSet.getString("email"));
            client.setId(resultSet.getLong("id"));
            return client;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
