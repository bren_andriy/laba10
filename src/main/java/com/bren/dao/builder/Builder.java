package com.bren.dao.builder;

import java.sql.ResultSet;

public interface Builder<T> {
    T build(ResultSet resultSet);
}
