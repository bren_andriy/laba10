package com.bren.dao.implementation;

import com.bren.constants.Messages;
import com.bren.constants.Queries;
import com.bren.dao.CrudDao;
import com.bren.dao.builder.ClientBuilder;
import com.bren.database.ConnectionManager;
import com.bren.entity.Client;
import com.bren.exception.NotFoundException;
import com.bren.util.JdbcUtil;
import lombok.Getter;
import lombok.Setter;

import java.sql.Connection;
import java.util.List;

@Setter
@Getter
public class ClientDao implements CrudDao<Client> {



    @Override
    public void save(Client client) {
        Connection connection = ConnectionManager.getInstance().getConnection();
        JdbcUtil.updateData(connection, Queries.INSERT_INTO_CLIENTS, client.getName(),
                client.getLastName(), client.getCityId(), client.getAge(),
                client.getPhone(),client.getEmail());
    }

    @Override
    public void update(Client client, Long id) {
        Connection connection = ConnectionManager.getInstance().getConnection();
        JdbcUtil.updateData(connection, Queries.UPDATE_CLIENT_BY_ID, client.getName(),
                client.getLastName(), client.getCityId(), client.getAge(),
                client.getPhone(),client.getEmail(), id);
    }

    @Override
    public List<Client> findAll() {
        Connection connection = ConnectionManager.getInstance().getConnection();
        return JdbcUtil.getEntityList(connection, Queries.FIND_ALL_CLIENTS, new ClientBuilder());
    }

    @Override
    public Client findById(Long id) {
        Connection connection = ConnectionManager.getInstance().getConnection();
        return JdbcUtil.getEntity(connection, Queries.FIND_CLIENT_BY_ID, new ClientBuilder(), id)
                .orElseThrow(() -> new NotFoundException(Messages.CLIENT_NOT_FOUND + id));

    }

    @Override
    public void delete(Long id) {
        Connection connection = ConnectionManager.getInstance().getConnection();
        JdbcUtil.updateData(connection, Queries.DELETE_CLIENT_BY_ID, id);
    }
}
