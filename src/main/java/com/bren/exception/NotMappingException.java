package com.bren.exception;

public class NotMappingException extends RuntimeException{
    public NotMappingException(String message) {
        super(message);
    }
}
