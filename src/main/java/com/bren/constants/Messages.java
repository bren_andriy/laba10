package com.bren.constants;

public interface Messages {
    String FAILED_CREATE_CONNECTION = "Failed to create the database connection.";
    String NO_MAPPING_FOR = "There are no mapping for";
    String CLIENT_NOT_FOUND = "Client not found with id: ";
}
