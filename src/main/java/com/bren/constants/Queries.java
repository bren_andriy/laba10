package com.bren.constants;

public interface Queries {
    String FIND_CLIENT_BY_ID = "select id, name, last_name, city_id, age, phone, email from clients where id = ?;";
    String FIND_ALL_CLIENTS = "select * from clients;";
    String INSERT_INTO_CLIENTS = "insert into clients (name, last_name, city_id, age, phone, email) values (?, ?, ?, ?, ?, ?);";
    String UPDATE_CLIENT_BY_ID = "update clients set name = ?, last_name = ?, city_id = ?, age = ?, phone = ?, email = ? where id = ?;";
    String DELETE_CLIENT_BY_ID = "delete from clients where id = ?;";
}
