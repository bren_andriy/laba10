package com.bren.constants;

public interface Parameters {
    String POSTGRESQL_DRIVER = "org.postgresql.Driver";
    String URL = "jdbc:postgresql://localhost:5432/commissioncarshop";
    String USER ="admin";
    String PASSWORD = "root";
}
