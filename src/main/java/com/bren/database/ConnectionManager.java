package com.bren.database;

import com.bren.exception.DatabaseFailedException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static com.bren.constants.Messages.FAILED_CREATE_CONNECTION;
import static com.bren.constants.Parameters.*;

public final class ConnectionManager {
    private static volatile ConnectionManager instance;
    private final Map<Long, Connection> connections;

    public ConnectionManager() {
        this.connections = new HashMap<>();
    }

    public static ConnectionManager getInstance() {
        if (instance == null) {
            synchronized (ConnectionManager.class) {
                instance = new ConnectionManager();
            }
        }
        return instance;
    }

    private Map<Long, Connection> getAllConnections() {
        return this.connections;
    }

    private void addConnection(Connection connection) {
        getAllConnections().put(Thread.currentThread().getId(), connection);
    }

    public Connection getConnection() {
        Connection connection = getAllConnections().get(Thread.currentThread().getId());
        if (connection == null) {
            try {
                Class.forName(POSTGRESQL_DRIVER);
                connection = DriverManager.getConnection(URL, USER, PASSWORD);
            } catch (SQLException | ClassNotFoundException ex) {
                throw new DatabaseFailedException(FAILED_CREATE_CONNECTION);
            }
            addConnection(connection);
        }
        return connection;
    }
}

