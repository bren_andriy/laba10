package com.bren.util;

import com.bren.constants.Messages;
import com.bren.dao.builder.Builder;
import com.bren.exception.NotMappingException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class JdbcUtil {
    public static <T> Optional<T> getEntity(Connection connection, String query,
                                            Builder<T> builder, Object... parameters) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            insertEntityParametersToStatement(preparedStatement, parameters);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(builder.build(resultSet));
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new RuntimeException();
        }
    }

    public static <T> List<T> getEntityList(Connection connection, String query,
                                            Builder<T> builder, Object... parameters) {
        List<T> entityList = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            insertEntityParametersToStatement(preparedStatement, parameters);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    entityList.add(builder.build(resultSet));
                }
                return entityList;
            }
        } catch (SQLException e) {
            throw new RuntimeException();
        }
    }

    public static void updateData(Connection connection, String query, Object... parameters) {
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            insertEntityParametersToStatement(statement, parameters);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    private static void insertEntityParametersToStatement(PreparedStatement statement,
                                                          Object... parameters) {
        try {
            for (int i = 0; i < parameters.length; i++) {
                if (parameters[i] == null) {
                    statement.setNull(i + 1, Types.NULL);
                } else if (parameters[i] instanceof Integer) {
                    statement.setInt(i + 1, (Integer) parameters[i]);
                } else if (parameters[i] instanceof Long) {
                    statement.setLong(i + 1, (Long) parameters[i]);
                } else if (parameters[i] instanceof String) {
                    statement.setString(i + 1, (String) parameters[i]);
                } else {
                    throw new NotMappingException(Messages.NO_MAPPING_FOR + parameters[i].getClass());
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
