package com.bren.view;

import com.bren.dao.CrudDao;
import com.bren.dao.implementation.ClientDao;
import com.bren.entity.Client;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ProgramView {
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner inputChoice = new Scanner(System.in);
    private Scanner input = new Scanner(System.in);
    private CrudDao<Client> clientCrudDao;

    public ProgramView() {
        clientCrudDao = new ClientDao();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Get Client By Id");
        menu.put("2", "  2 - Get All Clients");
        menu.put("3", "  3 - Add New Client");
        menu.put("4", "  4 - Update Client By Id");
        menu.put("5", "  5 - Delete Client By Id");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::findClientById);
        methodsMenu.put("2", this::findAllClients);
        methodsMenu.put("3", this::addNewClient);
        methodsMenu.put("4", this::updateClientById);
        methodsMenu.put("5", this::deleteClientById);


    }

    private void deleteClientById() {
        System.out.println("You want to delete client!");
        System.out.println("Input id: ");
        Long id = input.nextLong();
        clientCrudDao.delete(id);
    }

    private void updateClientById() {
        System.out.println("Input id: ");
        Long id = input.nextLong();
        Client client = clientCrudDao.findById(id);
        System.out.println("Input new Name: ");
        client.setName(input.next());
        System.out.println("Input age: ");
        client.setAge(input.nextInt());
        clientCrudDao.update(client, id);
    }

    private void addNewClient() {
        System.out.println("Input Name: ");
        String name = input.next();
        System.out.println("Input Last name: ");
        String lastName = input.next();
        System.out.println("Input City id: ");
        Long cityId = input.nextLong();
        System.out.println("Input age: ");
        Integer age = input.nextInt();
        System.out.println("Input phone: ");
        String phone = input.next();
        System.out.println("Input email: ");
        String email = input.next();
        Client client = new Client(name, lastName, cityId, age, phone, email);
        clientCrudDao.save(client);

    }

    private void findAllClients() {
        System.out.println("Clients: ");
        clientCrudDao.findAll().forEach(System.out::println);
    }

    private void findClientById() {
        System.out.println("Input id: ");
        Long id = Long.valueOf(input.next());
        System.out.println(id);
        System.out.println(clientCrudDao.findById(id).toString());
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = inputChoice.next().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
